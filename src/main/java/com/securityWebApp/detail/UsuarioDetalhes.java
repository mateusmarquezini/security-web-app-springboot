package com.securityWebApp.detail;

import com.securityWebApp.model.Usuario;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;

/**
 * Created by mmn on 25/10/16.
 */
public class UsuarioDetalhes extends Usuario implements UserDetails {

    private static final long serialVersionUID = 1L;

    private List<String> papeisUsuario;

    public UsuarioDetalhes(Usuario usuario, List<String> papeisUsuario) {
	super(usuario);
	this.papeisUsuario = papeisUsuario;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
	String papel = StringUtils.collectionToCommaDelimitedString(papeisUsuario);
	return AuthorityUtils.commaSeparatedStringToAuthorityList(papel);
    }

    public String getPassword() {
	return null;
    }

    public String getUsername() {
	return super.getNomeUsuario();
    }

    public boolean isAccountNonExpired() {
	return false;
    }

    public boolean isAccountNonLocked() {
	return false;
    }

    public boolean isCredentialsNonExpired() {
	return false;
    }

    public boolean isEnabled() {
	return false;
    }
}
