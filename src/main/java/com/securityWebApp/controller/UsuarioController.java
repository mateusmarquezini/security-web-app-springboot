package com.securityWebApp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by Mateus-PC on 24/10/2016.
 */
@Controller
public class UsuarioController {

    @RequestMapping("cadastroUsuario")
    public String cadastroUsuario() {
	return "cadastroUsuario";
    }
}
