package com.securityWebApp.service;

import com.securityWebApp.detail.UsuarioDetalhes;
import com.securityWebApp.model.Usuario;
import com.securityWebApp.repository.IUsuarioPapelRepository;
import com.securityWebApp.repository.IUsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mmn on 25/10/16.
 */
@Service("usuarioDetalhesService")
public class UsuarioDetalhesService implements UserDetailsService {
    @Autowired
    private IUsuarioRepository repositoryUsuario;

    @Autowired
    private IUsuarioPapelRepository repositoryUsuarioPapel;

    public UserDetails loadUserByUsername(final String nomeUsuario) throws UsernameNotFoundException {
	Usuario usuario = repositoryUsuario.findByNomeUsuario(nomeUsuario);

	if (usuario == null) {
	    throw new UsernameNotFoundException(String.format("Usuário %s não encontrado!", nomeUsuario));
	} else {
	    List<String> papeisUsuario = repositoryUsuarioPapel.findRoleByNomeUsuario(nomeUsuario);
	    return new UsuarioDetalhes(usuario, papeisUsuario);
	}
    }
}
