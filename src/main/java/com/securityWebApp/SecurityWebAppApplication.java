package com.securityWebApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories(basePackages = "com.securityWebApp.repository")
@EntityScan(basePackages = "com.securityWebApp.model")
@SpringBootApplication
public class SecurityWebAppApplication {

    public static void main(String[] args) {
	SpringApplication.run(SecurityWebAppApplication.class, args);
    }
}
