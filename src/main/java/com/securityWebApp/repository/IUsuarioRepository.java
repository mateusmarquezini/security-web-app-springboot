package com.securityWebApp.repository;

import com.securityWebApp.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by mateus marquezini on 25/10/16.
 */

@Repository
public interface IUsuarioRepository extends JpaRepository<Usuario, Long> {

    Usuario findByNomeUsuario(String nomeUsuario);
}
