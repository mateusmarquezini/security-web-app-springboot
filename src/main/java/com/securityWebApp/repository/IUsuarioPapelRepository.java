package com.securityWebApp.repository;

import com.securityWebApp.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by mmn on 25/10/16.
 */
@Repository
public interface IUsuarioPapelRepository extends JpaRepository<Usuario, Long> {

    @Query("SELECT a.papel FROM PapelUsuario a, Usuario b WHERE b.nomeUsuario=?1 AND a.usuarioId = b.id")
    List<String> findRoleByNomeUsuario(String nomeUsuario);
}
