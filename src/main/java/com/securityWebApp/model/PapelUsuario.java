package com.securityWebApp.model;

import javax.persistence.*;

/**
 * Created by mmn on 25/10/16.
 */
@Entity
@Table(name = "PAPEL_USUARIO")
public class PapelUsuario {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String papel;

    private Long usuarioId;

    public Long getId() {
	return id;
    }

    public String getPapel() {
	return papel;
    }

    public Long getUsuarioId() {
	return usuarioId;
    }

    public void setId(final Long pId) {
	id = pId;
    }

    public void setPapel(final String pPapel) {
	papel = pPapel;
    }

    public void setUsuarioId(final Long pUsuarioId) {
	usuarioId = pUsuarioId;
    }

}
