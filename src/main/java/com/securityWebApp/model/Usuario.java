package com.securityWebApp.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by mmn on 25/10/16.
 */

@Entity
@Table(name = "USUARIO")
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    private String email;

    private int habilitado;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String nomeUsuario;

    private String senha;

    public Usuario() {
    }

    public Usuario(Usuario usuario) {
	this.id = usuario.id;
	this.nomeUsuario = usuario.nomeUsuario;
	this.email = usuario.email;
	this.senha = usuario.senha;
	this.habilitado = usuario.habilitado;
    }

    public String getEmail() {
	return email;
    }

    public int getHabilitado() {
	return habilitado;
    }

    public Long getId() {
	return id;
    }

    public String getNomeUsuario() {
	return nomeUsuario;
    }

    public String getSenha() {
	return senha;
    }

    public void setEmail(final String pEmail) {
	email = pEmail;
    }

    public void setHabilitado(final int pHabilitado) {
	habilitado = pHabilitado;
    }

    public void setId(final Long pId) {
	id = pId;
    }

    public void setNomeUsuario(final String pNomeUsuario) {
	nomeUsuario = pNomeUsuario;
    }

    public void setSenha(final String pSenha) {
	senha = pSenha;
    }

}

